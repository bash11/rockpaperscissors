import React from 'react';
import ReactDOM from 'react-dom';
import Game from './pages/game';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Game />, document.querySelector('#root'));
registerServiceWorker();
