export default theme => ({
  root: {
    textAlign: 'center',
    paddingTop: 20, //theme.spacing.unit * 
  },
  choice: {
    //display: 'table-cell',
    //width: '25%'
    padding: 10
  },
  choicepc: {
    display: 'table-cell',
    cursor: 'default'
  },
  typog: {
    marginTop: 15
  },
  score: {
    marginTop: 15,
    marginBottom: 20,
    fontWeight: 'bold'
  },
  player: {
    marginTop: 5,
    marginBottom: 15,
    fontWeight: 'bold',
    fontSize: 26
  },
  icon: {
    marginLeft: 10,
    marginRight: 10,
  },
  winner: {
    color: 'green',
    fontSize: 26
  },
  playagain: {
    color: 'purple',
  },
  tie: {
    fontSize: 26
  },
  pickone: {
    color: 'red',
    fontSize: 22,
    marginBottom: 15,
  },
  butcont: {
    marginBottom:30,
    textAlign: 'center',
  },
});