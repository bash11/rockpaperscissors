
import React from 'react';
import {shallow} from 'enzyme';
import Game from "./Game";
import {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

let props;


beforeAll(() => {
   global.localStorage = {
      i2x_token: 'someToken',
      getItem: function () {
         return '{"open":false,"started":true,"score1":3,"score2":0,"choiceInterval":0,"withHuman":1,"player1":"You","player2":"PC","winner1":true,"winner2":false,"playagain":true,"tie":false,"selection":true,"fade1":[false,false,true],"fade2":[false,true,false],"pickone":1}'
      }
   };
});


it('should render without crashing', () => {
  mount(<Game props />);
});

