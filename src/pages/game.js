import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../withRoot';

import Grid from '@material-ui/core/Grid';
import Fade from '@material-ui/core/Fade';

import styles from './main.component.style.js';

require('array.prototype.fill');




function ChoiceButton(props){
  return (
            <Fade in={props.fade}>
              <a className={props.class} href="javascript:void(0);" onClick={props.onClick}><img width={props.imgwidth} src={props.imgname} /></a>
            </Fade>
          );
}


export class Board extends React.Component{


  renderButton(i, classname, choicevalue, imgwidth, imgname){
    return (
              <ChoiceButton 
                fade={this.props.fade[i]}
                class={classname}
                imgwidth={imgwidth}
                imgname={imgname}
                onClick={ () => this.props.onClick(choicevalue) }
              />
            );
  }


  render(){

    return (

      <div>
          {this.renderButton(0, "Game-choice-02", 1, '26%', "images/rock.png")}
          {this.renderButton(1, "Game-choice-02", 2, '26%', "images/paper.png")}
          {this.renderButton(2, "Game-choice-02", 3, '26%', "images/scissors.png")}
      </div>

      
    );
  }

}




export class Game extends React.Component {

  constructor(props){
    super(props);

    var fstate = {
      open: false,
      started: false,
      score1: 0,
      score2: 0,
      choiceInterval: 0,
      withHuman: 0,
      player1: '',
      player2: '',
      winner1: false,
      winner2: false,
      playagain: false,
      tie: false,
      selection: true,
      fade1: Array(3).fill(true),
      fade2: Array(3).fill(true),
      pickone: 1
    };

    this.state = JSON.parse(localStorage.getItem( 'state' ) ) || fstate;

  }

  

  startGame = (mode) => {
    let whuman = 0;
    let player1 = 'PC1';
    let player2 = 'PC2';
    let selection = true;

    // if Me Vs PC
    if(mode){
      whuman = 1;
      player1 = 'You';
      player2 = 'PC';
      selection = false;
    }

    this.setState({
      started: true,
      choiceInterval: 1,
      withHuman: whuman,
      player1: player1,
      player2: player2,
      selection: selection
    });

    // if PC1 Vs PC2 automatically select choices
    if(!mode){
      this.autoChoice();
    }
  };

  resetGame = () => {
    const fade1 = Array(3).fill(true);
    const fade2 = Array(3).fill(true);

    this.setState({
      started: false,
      score1: 0,
      score2: 0,
      playagain: false,
      winner1: false,
      winner2: false,
      tie: false,
      fade1: fade1,
      fade2: fade2,
      selection: true
    });
    this.setState({choiceInterval: 0});

    localStorage.removeItem('state');
  };

  playAgain = () => {
    const fade1 = Array(3).fill(true);
    const fade2 = Array(3).fill(true);

    this.setState({
      playagain: false,
      winner1: false,
      winner2: false,
      tie: false,
      fade1: fade1,
      fade2: fade2,
      selection: false
    });

    const mode = this.state.withHuman;
    // if PC1 Vs PC2 automatically select choices
    if(!mode){
      this.autoChoice();
    }

  }


  // PC1 Vs. PC2, randomly selected input
  autoChoice = () => {
    let rand1 = this.getRand();
    let rand2 = this.getRand();

    this.processSelection(rand1, rand2);
  }

  // Me Vs. PC, human selected input
  selectChoice = (choice, board) => {
    if(!this.state.selection && board == 1){
      let rand = this.getRand();

      this.processSelection(choice, rand);
    }
  }


  // update the state after selection and before finding the winner
  processSelection = (a, b) => {
    const fade1 = Array(3).fill(false);
    fade1[a-1] = true;

    const fade2 = Array(3).fill(false);
    fade2[b-1] = true;

    this.setState({
        fade1: fade1,
        fade2: fade2,
        selection: true,
      });
    
    this.stageWinner( a, b );
  }

  //generate random number between 1 & 3
  getRand = () => {
    return Math.floor(Math.random() * 3) + 1;
  }


  //calculate winner given that rock = 1, paper = 2, scissors = 3. a & b are the input values of the 2 players
  stageWinner = (a, b) => {
    //console.log(a, b);

    this.setState({choiceInterval: 0});
    this.setState({playagain: true});

    let retval = "";
    if ((b) % 3 + 1 == a){
      this.setState({
        score1: this.state.score1 + 1,
        winner1: true,
      }, () => {
          localStorage.setItem( 'state', JSON.stringify(this.state)  );
      });
      retval = "one";
    }else if ((a) % 3 + 1 == b){
      this.setState({
        score2: this.state.score2 + 1,
        winner2: true,
      }, () => {
          localStorage.setItem( 'state', JSON.stringify(this.state)  );
      });
      retval = "two";
    }else{
      this.setState({tie: 1});
      retval = "Draw";
    }

    
    
    return retval;
  }




  render() {
    const {classes } = this.props;
    const {open } = this.state;
    const {started} = this.state;
    const {score1} = this.state;
    const {score2} = this.state;
    const {winner1} = this.state;
    const {winner2} = this.state;
    const {playagain} = this.state;
    const {tie} = this.state;
    const {fade1} = this.state;
    const {fade2} = this.state;
    const {pickone} = this.state;
    const {selection} = this.state;
    

    return (
      <div className={classes.root}>

        <Grid container spacing={24}>
        <Grid item xs>
          
        </Grid>
        <Grid item xs>
          <Fade in={started}>
            <Button variant="contained" color="secondary" onClick={this.resetGame}>
              Start Over
            </Button>
          </Fade>
          
        </Grid>
        <Grid item xs>
           
        </Grid>
      </Grid>


        
        <Typography variant="display1" className={classes.typog} gutterBottom>
          Play Rock-Paper-Scissors!
        </Typography>

        <Fade in={playagain}>
          <Typography gutterBottom>
            <a className={classes.playagain} href="javascript:void(0);" onClick={() => this.playAgain()}>Play Again?</a>
          </Typography>
        </Fade>

        <Fade in={tie}>
          <Typography className={classes.tie} gutterBottom>
            Tie
          </Typography>
        </Fade>

      

      <Fade in={!started}>
        <Grid container spacing={24}  style={{display: !this.state.started ? '' : 'none' }}>
          <Grid item xs>

          </Grid>
          <Grid item xs={6}>
              <Grid className={classes.butcont} container spacing={8} >

                <Grid item xs={6}>
                    <Fade in={!started}>
                      <Button variant="contained" color="primary" onClick={ () => this.startGame(1)}>
                        Me Vs. PC
                      </Button>
                    </Fade>
                    
                </Grid>
                <Grid item xs={6}>
                    <Fade in={!started}>
                      <Button variant="contained" color="primary" onClick={() => this.startGame(0)}>
                        PC Vs. PC
                      </Button>
                    </Fade>
                </Grid>
              </Grid>

            <Board 
                fade={fade2}
                onClick={(i) => this.selectChoice(i, 2)}
            />

          </Grid>
          <Grid item xs>

          </Grid>
        </Grid>
      </Fade>

      <Fade in={started}  style={{display: this.state.started ? '' : 'none' }}>
        <Grid container spacing={24} >
          <Grid item xs>

          </Grid>
          <Grid item xs={4}>
            <Fade in={winner1}>
              <Typography className={classes.winner} variant="subheading" gutterBottom>
                Winner!
              </Typography>
            </Fade>
            <Typography className={classes.player} variant="subheading" gutterBottom>
              Player : {this.state.player1}
            </Typography>
            <Typography className={classes.score} variant="subheading" gutterBottom>
              Score: {this.state.score1}
            </Typography>
            <Fade in={!selection}>
              <Typography className={classes.pickone} variant="subheading" gutterBottom>
                Pick One!
              </Typography>
            </Fade>


            <Board 
                fade={fade1}
                onClick={(i) => this.selectChoice(i, 1)}
            />

          </Grid>
          <Grid item xs={4}>
            <Fade in={winner2}>
              <Typography className={classes.winner} variant="subheading" gutterBottom>
                Winner!
              </Typography>
            </Fade>
            <Typography className={classes.player} variant="subheading" gutterBottom>
              Player : {this.state.player2}
            </Typography>
            <Typography className={classes.score} variant="subheading" gutterBottom>
              Score: {this.state.score2}
            </Typography>
            <Fade in={!selection}>
              <Typography className={classes.pickone} variant="subheading" gutterBottom>
                &nbsp;
              </Typography>
            </Fade>

            <Board 
                fade={fade2}
                onClick={(i) => this.selectChoice(i, 2)}
            />

          </Grid>

          <Grid item xs>

          </Grid>
        </Grid>
      </Fade>


      


      </div>
    );
  }
}

Game.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(Game));
